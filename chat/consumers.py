from channels import Channel, Group
from channels.sessions import channel_session
from channels.auth import channel_session_user, channel_session_user_from_http
#from channels.security.websockets import allowed_hosts_only

# Connected to websocket.connect
def ws_add(message):
    # Accept the connection
    message.reply_channel.send({"accept": True})
    # Add to the chat group
    Group("chat").add(message.reply_channel)

# Connected to websocket.receive
def ws_message(message):
    Group("chat").send({
        "text": "[user] %s" % message.content['text'],
    })

# Connected to websocket.disconnect
def ws_disconnect(message):
    Group("chat").discard(message.reply_channel)
# Connected to websocket.connect
#@allowed_hosts_only
#@channel_session_user_from_http
#def ws_add(message):
#    # Accept connection
#    message.reply_channel.send({"accept": True})
#    # Add them to the right group
#    Group("chat-%s" % message.user.username[0]).add(message.reply_channel)
#
## Connected to websocket.receive
##@allowed_hosts_only
#@channel_session_user
#def ws_message(message):
#    Group("chat-%s" % message.user.username[0]).send({
#        "text": message['text'],
#    })
#
## Connected to websocket.disconnect
##@allowed_hosts_only
#@channel_session_user
#def ws_disconnect(message):
#    Group("chat-%s" % message.user.username[0]).discard(message.reply_channel)
