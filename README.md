We recommend to use a virtualenv for python. If you have python3 installed, it should had come with it.  
After you have cloned the repository, you can execute the following commands in your shell:
```[sh]
cd /path/to/folder
virtualenv --python=python3 .env
source .env/bin/activate
pip install -r requirements.txt
./manage.py runserver
```